INSERT INTO Produkty(rodzina,rzad,nazwa,ilosc,cena) VALUES ('astrowate','astrowce','kocanki ogrodowe',10,8.99);
INSERT INTO Produkty(rodzina,rzad,nazwa,ilosc,cena) VALUES ('astrowate','astrowce','cynia wytworna',1,9.60);
INSERT INTO Produkty(rodzina,rzad,nazwa,ilosc,cena) VALUES ('szkarlatowate','gozdzikowce','celozja srebrzysta',89,1.70);
INSERT INTO Produkty(rodzina,rzad,nazwa,ilosc,cena) VALUES ('begoniowate','dyniowce','begonia stale kwitnaca',24,12.20);
INSERT INTO Produkty(rodzina,rzad,nazwa,ilosc,cena) VALUES ('astrowate','astrowce','aster chinski',3,12.80);
INSERT INTO Produkty(rodzina,rzad,nazwa,ilosc,cena) VALUES ('jasnotowate','jasnotowce','dzwonki irlandzkie',15,2.10);
INSERT INTO Produkty(rodzina,rzad,nazwa,ilosc,cena) VALUES ('wielosilowate','wrzosowce','floks drummonda',41,6.50);
INSERT INTO Produkty(rodzina,rzad,nazwa,ilosc,cena) VALUES ('astrowate','astrowce','gazania lsniaca',2,16.11);
INSERT INTO Produkty(rodzina,rzad,nazwa,ilosc,cena) VALUES ('wielosilowate','wrzosowce','kobea pnaca',17,12.25);


INSERT INTO Filia(kraj,miasto,ulica,lokal) VALUES('Polska','Wroclaw','Sienkiewicza','24a/3');
INSERT INTO Filia(kraj,miasto,ulica,lokal) VALUES('Rosja','Moskwa','Dostojewskiego','32/2');
INSERT INTO Filia(kraj,miasto,ulica,lokal) VALUES('Austria','Wieden','KaiserStrasse','123/2f');
INSERT INTO Filia(kraj,miasto,ulica,lokal) VALUES('Polska','Wroclaw','Debskiego','246a/12');
INSERT INTO Filia(kraj,miasto,ulica,lokal) VALUES('Wlochy','Neapol','Bernoulli','6g/2');
INSERT INTO Filia(kraj,miasto,ulica,lokal) VALUES('USA','New York City','45-th Street','234/1');
INSERT INTO Filia(kraj,miasto,ulica,lokal) VALUES('Hiszpania','Barcelona','Sagrada Familia','1');


INSERT INTO Telefon(numer,ID_filia) VALUES(192837234,2);
INSERT INTO Telefon(numer,ID_filia) VALUES(123982382,2);
INSERT INTO Telefon(numer,ID_filia) VALUES(892838388,2);
INSERT INTO Telefon(numer,ID_filia) VALUES(131323234,1);
INSERT INTO Telefon(numer,ID_filia) VALUES(232313234,3);
INSERT INTO Telefon(numer,ID_filia) VALUES(929323884,4);
INSERT INTO Telefon(numer,ID_filia) VALUES(238238238,2);
INSERT INTO Telefon(numer,ID_filia) VALUES(123982323,1);

INSERT INTO Pracownicy(PESEL,data_zatrudnienia,imie,ID_filia,nazwisko,pensja,funkcja) VALUES ("19283747121",'1997-12-11','Aneta',1,'Jozwik',4500,'sprzedawca');
INSERT INTO Pracownicy(PESEL,data_zatrudnienia,imie,ID_filia,nazwisko,pensja,funkcja) VALUES ("19293939291",'2017-10-19','Anna',2,'Maria',3500,'ksiegowa');
INSERT INTO Pracownicy(PESEL,data_zatrudnienia,imie,ID_filia,nazwisko,pensja,funkcja) VALUES ("19002939348",'1990-02-01','Ludomir',3,'Anastej',8000,'wlasciciel');
INSERT INTO Pracownicy(PESEL,data_zatrudnienia,imie,ID_filia,nazwisko,pensja,funkcja) VALUES ("18382382382",'1999-09-21','Jakub',4,'Weranda',2600,'sprzedawca');
INSERT INTO Pracownicy(PESEL,data_zatrudnienia,imie,ID_filia,nazwisko,pensja,funkcja) VALUES ("12323238181",'2005-10-11','Paulina',3,'Anastej',6500,'stazysta');


INSERT INTO Zawartosc(ID_zamowienia,ID_produkt,ilosc,cena_kupna) VALUES(0,5,10,17.20);
INSERT INTO Zawartosc(ID_zamowienia,ID_produkt,ilosc,cena_kupna) VALUES(0,2,11,17.20);
INSERT INTO Zawartosc(ID_zamowienia,ID_produkt,ilosc,cena_kupna) VALUES(0,6,11,17.20);
INSERT INTO Zawartosc(ID_zamowienia,ID_produkt,ilosc,cena_kupna) VALUES(1,1,11,17.20);
INSERT INTO Zawartosc(ID_zamowienia,ID_produkt,ilosc,cena_kupna) VALUES(3,2,11,17.20);

INSERT INTO Zamowienia(ID_sprzedawcy,ID_zawartosc,ID_filia,potwierdzenie,data_sprzedazy,platnosc_karta,kwota) VALUES (1,0,3,False,'2017-11-11',False,12.20);
INSERT INTO Zamowienia(ID_sprzedawcy,ID_zawartosc,ID_filia,potwierdzenie,data_sprzedazy,platnosc_karta,kwota) VALUES (1,1,3,False,'2017-11-11',False,12.20);
INSERT INTO Zamowienia(ID_sprzedawcy,ID_zawartosc,ID_filia,potwierdzenie,data_sprzedazy,platnosc_karta,kwota) VALUES (1,2,3,False,'2017-11-11',True,12.20);
INSERT INTO Zamowienia(ID_sprzedawcy,ID_zawartosc,ID_filia,potwierdzenie,data_sprzedazy,platnosc_karta,kwota) VALUES (1,3,3,True,'2017-11-11',False,12.20);
