import pymysql

class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

#Python3 Singleton based on metaclass
class CommunicationHandler(metaclass=Singleton):
    conn = None
    lastUsername = ""
    lastPassword = ""
    lastHost = ""
    lastDatabase = ""
    lastSelectColumns =[]
    lastSelectTable =""

    #TODO: implement some SSL stuff?
    def connect(self, username, password, host='127.0.0.1', database='flower_power'):
        try:
            connection_successful=False
            self.conn = pymysql.connect(user=username, password=password, host=host, database=database)
            print("connection successful")
            connection_successful=True
        except pymysql.MySQLError as e:
            print("could not connect")
            print (str(e.args[0])+" -> "+str(e))
            return str(e.args[0])
        finally:
            if connection_successful:
                self.lastUsername=username
                self.lastPassword=password
                self.lastHost=host
                self.lastDatabase=database
        return "success"

    def disconnect(self):
        #TODO: do i even have to disconnect anything? 
        try:
            self.conn.close()
        except:
            print("Error while closing")
        self.lastUsername = ""
        self.lastPassword = ""
        self.lastHost = ""
        self.lastDatabase = ""

    def export_database(self):
        pass

    def submit_order(self,ids,count):
        order_id = int(self.query("SELECT `AUTO_INCREMENT` FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'flower_power' AND   TABLE_NAME   = 'Zamowienia'").fetchone()[0]);

       
        column_names=["ID_sprzedawcy","ID_filia","potwierdzenie","data_sprzedazy","platnosc_karta"]
        columns =["1","2","0","0"] #this is hardcoded, but should not be
        try:
            self.insert_to_database(column_names,columns,"Zamowienia")

            for x in range(0,len(ids)):
                columns_names=["ID_zamowienia","ID_produkt","ilosc","cena_kupna"]
                columns=[str(order_id),ids[x],count[x],float(self.query("SELECT cena FROM Produkty WHERE ID="+ids[x]).fetchone()[0])]
                self.insert_to_database(columns_names,columns,"Zawartosc")

        except:
            return False

        return True

    def query(self, sql):
        try:
            cursor = self.conn.cursor()
            cursor.execute(sql)
            cursor.close()
        except (AttributeError, pymysql.OperationalError):
            self.connect(self.lastUsername, self.lastPassword)
            cursor = self.conn.cursor()
            cursor.execute(sql)
            cursor.close()
        return cursor
    
    #zwraca liste kolumn w tabeli jako tuple
    def get_all_columns(self, table):
        sql="SELECT `COLUMN_NAME` FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" +self.lastDatabase+"' AND TABLE_NAME = '"+table+"'"
        
        print(sql) #DEBUG
        response=self.query(sql).fetchall()
        return response

    #zwraca liste tabeli w bazie jako tuple
    def get_all_tables(self):
        response=self.query("SHOW TABLES").fetchall()
        print(response) #DEBUG
        return response

    def check_columns(self, columns, table):
        #table name is set to world because of debugging, change it to flower_power when you implement it...
        response=self.get_all_columns(table)
        print(response) #DEBUG
        
        how_many_args = len(columns)
        correct_args=0
        
        for x in columns:
            for y in response:
                if x in y:
                    correct_args+=1
                    continue
       
        if correct_args==how_many_args:
            return True
        else:
            return False
    
    def select(self, columns, table):
        correct_data=self.check_columns(columns,table);
        if correct_data:
            command = "SELECT ";
            for x in columns:
                command+=x
                command+=" ,"
            command = command[0:len(command)-1]
            command += "FROM "
            command += table
            print(command)
            response =self.query(command).fetchall()
            self.lastSelectColumns=columns;
            self.lastSelectTable=table;
            return response


        return "incorrect data"
    def perform_procedure(self, procedure_name):
        #hardcode procedures and return output
        # jesli cos pojdzie zle to "incorrect stuff"
        pass

    def insert_to_database(self, column_names, columns,table):
        print(columns)
        print(table)

        sql = "INSERT INTO "+table+"("
        for x in range(0,len(column_names)):
            if(columns[x]!="X"):
                sql+=column_names[x]
                sql+=","

        sql = sql[0:len(sql)-1]
        sql +=") VALUES("
        for x in columns:
            if x!="X":
                if isinstance(x,str):
                    sql+="'"
                sql+=x
                if isinstance(x,str):
                    sql+="'"
                sql+=","

        sql = sql[0:len(sql)-1]
        sql +=")"

        print(sql) #DEBUG
       
        try:
            response = self.query(sql).fetchall()
        except Exception as e:
            print(e)
            return False
        print(response)

        return True
    def remove_from_database(self,columns_names,columns,table):
        sql ="DELETE FROM "+table +" WHERE "
        for x in range(0,len(columns)):
            try:
                print(columns[x])
                columns[x]=int(columns[x])
                print("INTEGER")
            except ValueError:
                try:
                    columns[x]=float(columns[x])
                    print("FLOAT")
                except ValueError:
                    print("STRING")
            if isinstance(columns[x],str):
                temp=True
            else:
                temp=False
            sql+=columns_names[x]
            sql+="="
            if temp:
                sql+="'"
            sql+=str(columns[x])
            if temp:
                sql+="'"
            sql+=" AND "

        sql = sql[0:len(sql)-4]
        print(sql)

        try:
            response = self.query(sql).fetchall()
        except Exception as e:
            print(e)
            return False
        print(response)

        return True


    def get_all_fields(self,table):
        command = "SELECT * FROM " +table
       
        response =self.query(command).fetchall()
        return response

if __name__=="__main__":
   CommunicationHandler().connect("root","")
   columns = ['ID','imie','nazwisko']
   print(CommunicationHandler().select(columns, "Pracownicy"))
   CommunicationHandler().insert_to_database(["numer","ID_filia"],["192939495","15"],"Telefon")
